
Requirements
-------------
* Laravel 5.8
* PHP 7.x
* Mysql
* Apache
* composer
* npm


App Installation 
-------------
The installation requires composer and npm. Please install these requirements before continuing. 

1. CD to your apache document root and clone repo.
    ```
    > git clone https://feedersticks@bitbucket.org/feedersticks/flickr.git
    ```
2. Install composer and npm packages.
    ```
    > composer install
    > npm install
    ```
3. Create .env file and generate key.
    ```
    > cp .env.example .env
    > php artisan key:generate
    ```
4. Create new database and database user for the App. 
5. Edit .env and configure database and flickr settings.

```

    #Database settings
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=<YOUR-DB-NAME>
    DB_USERNAME=<YOUR-DB-USER>
    DB_PASSWORD=<YOUR-DB-USER-PASSWORD>

    #Flickr settings 
    FLICKR_KEY=4ee601e7ee9d61b351d46ea82d29a335
    FLICKR_SECRET=b46941d4bcd3732c

```


#Local domain and Apache Configuration
-------------
1. Create a localhost domain alias `flickr.local`. Edit /etc/hosts and add new domain alias
```
    # flickr local domain
    127.0.0.1  flickr.local

```
2. Configure Apache Virtual hosts. The document root of the app should be set to the public folder.
```
    <VirtualHost *:80>
    DocumentRoot "<YOUR-APACHE-HTDOC-DIR>/flickr/public/"
    ServerName ali.local
    DirectoryIndex index.php
    <Directory "<YOUR-APACHE-HTDOC-DIR>/flickr/public/">
        AllowOverride All
        Allow from All
    </Directory>
    </VirtualHost>

```


#Admin
-------------
Access `http://flickr.local/admin` and login using the default user.

* username: admin_user@test.com
* password: password1234

