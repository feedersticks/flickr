<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // define admin credentials here.
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin_user@test.com',
            'password' => bcrypt('password1234'),
        ]);
    }
}
