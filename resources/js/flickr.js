$(document).ready(function(){
  
  /** Event Listners */
  
  // For md and up resolution - show the category list
  // Sidebar menu event listner 
  $('.category-item').on('click', function(){
    $('.category-item').removeClass('active');
    let categoryID = $(this).addClass('active').attr('data-category-id');
    getGallery(categoryID);
    $('#select-category-item').val(categoryID);
  })

  // For sm and down resolution - replace category list with a drop down selection
  // This should mimic the category item state
  $('#select-category-item').on('change', function() {
    $('.category-item[data-category-id=' + $(this).val() + ']').trigger('click');
  })

  // Gallery item event listener
  $('#result-list > #images').on('click', '.gallery-img', function() {
    let photoID = $(this).attr('data-image-id')
    getPhotoInfo(photoID);
  })

  // Show more event listner
  $('#show-more').on('click', function() {
    let categoryID = $('#select-category-item').val();
    let next_page = $(this).attr('data-next-page');
    getGallery(categoryID, next_page);
  })

  // PhotoInfo close button event listener
  $('#close-selected').on('click', function() {
    $('#photo-info').hide();
    $('#result-list').show();
    $('#photo-selected').attr('src','');
  })


  /**
   * Ajax request for Category Gallery
   * 
   * @param {int} id category ID
   * @param {int} p page number of the gallery
   */

  function getGallery(id, p) {

    // set to 1 if the page is not defined
    let page = (p === undefined) ? 1 : p;

    $.ajax({
      url: "api/gallery/" + id +'/' + page,
      dataType: "json",
      beforeSend: function() {
          // hide gallery, error, and show loading message
          $('#photo-info').hide();
          if(page === 1) {
            $('#loading').show();
            $('#result-list').hide();
            $('#error').hide();
          }
      },
      success: function(data) {
        // empty gallery
        if(page === 1) {
          $('#result-list #images').empty();
        }
        
        if (data.photos.length > 0) {
          // append images
          data.photos.forEach(photo => {
            var html_template = '<div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">' +
                                ' <img src="' + photo.imageUrls.largeSquare + '" alt="' + photo.title + '" class="center-block img-thumbnail img-responsive gallery-img" data-image-id="' + photo.id +'">' + 
                                '</div>';
            $('#result-list #images').append(html_template);
          });

        }else{
          $('#result-list #images').append('<div class="col-xs-12"><div class="alert alert-danger" role="alert">No results found.</div></div>');
        }

        // update gallery name
        $('.category-title').html(data.category);

        //update next button
        if (data.next_page) {
          $('#show-more').attr('data-next-page', data.next_page);
          $('#show-more').show()
        }else{
          $('#show-more').hide()
        }
        
        // Once done with the request and rendering dom elements, hide loading message and show gallery.
        $('#loading').hide();
        $('#result-list').show();

      },
      error: function() {
        // Error handler if ever there's a server issue.
        $('#loading').hide();
        $('#result-list').hide();
        $('#error').show();
      }});
  }

  /**
   * Ajax request to get Photo Info
   * 
   * @param {int} id Photo ID
   */
  function getPhotoInfo(id) {
    $.ajax({
      url: "api/photo/" + id,
      dataType: "json",
      beforeSend: function() {
        //  hide gallery, error, and show loading message
          $('#loading').show();
          $('#photo-info').hide();
          $('#result-list').hide();
      },
      success: function(data) {
        // fill the placeholder with the image details
        $('#photo-selected').attr({
          'src': data.imageUrls.large1024,
          'alt': data.title
        });
        $('#photo-title').html(data.title)
        $('#photo-description').html(data.description)
        $('#photo-author').html(data.owner.username)
        $('#photo-author-img').attr('src', '//flickr.com/buddyicons/'+ data.owner.nsid +'.jpg')
        $('#photo-posted').html(data.dates.posted)

        // Once the data is set,  hide loading and show photo info
        $('#loading').hide();
        $('#photo-info').show();
        $('#result-list').hide();
      },
      error: function() {
        // error handler
        $('#loading').hide();
        $('#result-list').hide();
        $('#error').show();
      }});
  }

  // on page load, get the first category gallery
  // trigger click to load the first category
  $('.category-item:eq(0)').trigger('click');
});
