@extends('template')

@section('main')


@if( count($categories) == 0)

<div class="row">
    <div class="col-md-12">
        <h1>Service not available. </h1>
        <div class="alert alert-danger">
            <strong>Error!</strong> No photo category available. Please contact the admin.
        </div>
    </div>
</div>

@else

<div class="row">
    <div class="col-sm-12 text-center gallery-title">
        <h2>
            <span class="category-title"></span> Photo Gallery
        </h2>
    </div>
</div>

<div class="row">

    <div class="col-md-3 col-lg-3 hidden-xs hidden-sm ">
        <div class="list-group ">
            <a href="#" class="list-group-item disabled">
                <strong>Select Category</strong>
            </a>
            @foreach ($categories as $category)
            <a href="#{{ $category->id }}" class="list-group-item category-item" data-category-id="{{ $category->id }}">{{ $category->name }}</a>
            @endforeach
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 visible-xs visible-sm">
        <div class="form-group">
            <label class="control-label">Select Category</label>
            <select class="form-control" id="select-category-item">
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }} </option>
                @endforeach
            </select>
        </div>    
    </div>

    <div class="col-xs-12  col-sm-12 col-md-9 col-lg-9">
        <div id="result-container">
            <!-- gallery list -->
            <div id="result-list">
                <div id="images"></div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <button class="btn btn-default btn-block" id="show-more" data-next-page="1">Show more</button>
                </div>
            </div>

            <!-- ./gallery list -->

            <!-- photo details -->
            <div id="photo-info">
                <i class="icon glyphicon glyphicon-remove" id="close-selected"></i>
                <div class="col-md-8">
                    <img src="#" class="center-block img-responsive block" id="photo-selected"/>
                </div>
                <div class="col-md-4">
                    <h4 id="photo-title"></h4>
                    <small class="text-muted">
                        <img src="" width="25" id="photo-author-img" height="25" class="img-circle">
                        <strong id="photo-author"></strong> 
                    </small>
                    <p class="text-muted" id="photo-description"></p>
                </div>
            </div>
            <!-- ./photo details -->
            
            <!-- loader -->
            <div id="loading">
                <div class="col-md-12">
                    <small class="text-muted">Loading, please wait...</small>
                </div>
            </div>
            <!-- ./loader -->

            <!-- error -->
            <div id="error">
                <div class="col-md-12">
                    <strong>An error occured. Please try again.</strong>
                </div>
            </div>
            <!-- error -->
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endif

@endsection
