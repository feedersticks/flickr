@extends('admin') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="title">Update Category</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('gallery.update', $category->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">
                <label for="name">Category Name:</label>
                <input type="text" class="form-control" name="name" value={{ $category->name }} />
            </div>
            <div class="form-group">
                <label for="description">Category Description:</label>
                <textarea class="form-control" name="description">{{ $category->description }} </textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
            <a href="{{ route('gallery.index') }}" class="btn btn-link">Back</a>
        </form>
    </div>
</div>
@endsection