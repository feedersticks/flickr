<?php

namespace App\Http\Controllers;

use App\GalleryCategory;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $categories = GalleryCategory::all();
        return view('gallery.index', compact('categories')); 
    }
}
