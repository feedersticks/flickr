<?php

namespace App\Http\Controllers;

use App\GalleryCategory;
use App\Helpers\FlickrHelper;

class ApiGalleryController extends Controller
{
    /**
     * @param $id category ID
     * @return array
     */
    public function show($id, $page)
    {
        $category = GalleryCategory::find($id);

        $results = FlickrHelper::getSearchResults($category->name, $page);

        $next_page = ($results->photos['pages'] > $results->photos['page'] ? $results->photos['page'] + 1 : false);
       
        $response = [
            'category' => $category->name,
            'next_page' => $next_page,
            'photos' => []
        ];

        foreach ($results->photos['photo'] as $photo) {
            if ($photo['farm'] != 0) {
                $urls = FlickrHelper::generatePhotoSrc($photo);
                $title = $photo['title'];
                $response['photos'][] = [
                    'id' => $photo['id'],
                    'imageUrls' => $urls,
                    'title' => $title,
                ];
            }
        }

        return $response;
    }

    /**
     * @param $id photo ID
     * @return array
     */
    public function photoInfo($photoId)
    {
        $result = FlickrHelper::getPhotoInfo($photoId);
        $response = [
            'id' => $result->photo['id'],
            'imageUrls' => FlickrHelper::generatePhotoSrc($result->photo),
            'title' => $result->photo['title']['_content'],
            'description' => $result->photo['description']['_content'],
            'owner' => $result->photo['owner'],
            'dates' => $result->photo['dates']
        ];

        return $response;
    }
}
