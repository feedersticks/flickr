<?php

namespace App\Helpers;


use JeroenG\Flickr\FlickrLaravelFacade as Flickr;

class FlickrHelper
{
    /**
     * @param $searchString search string
     * @param int $page Page number
     * @param int $perPage Number of items per page
     * @return mixed
     */
    public static function getSearchResults($searchString, $page = 1, $perPage = 18)
    {
        $parameters = ['text' => $searchString, 'page' => $page, 'per_page' => $perPage];
        return Flickr::request('flickr.photos.search', $parameters);
    }

    public static function getPhotoInfo($photoId)
    {
        $parameters = ['photo_id' => $photoId];
        return Flickr::request('flickr.photos.getInfo', $parameters);
    }

    /**
     * Generate the photo URL based on the flickr format for static image
     *
     * https://www.flickr.com/services/api/misc.urls.html
     * The letter suffixes are as follows:
     * s	small square 75x75
     * q	large square 150x150
     * t	thumbnail, 100 on longest side
     * m	small, 240 on longest side
     * n	small, 320 on longest side
     * -	medium, 500 on longest side
     * z	medium 640, 640 on longest side
     * c	medium 800, 800 on longest side†
     * b	large, 1024 on longest side*
     * h	large 1600, 1600 on longest side†
     * k	large 2048, 2048 on longest side†
     * o	original image, either a jpg, gif or png, depending on source format
     * http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
     * http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
     *
     * @param $photoId
     * @return string
     */
    public static function generatePhotoSrc($photoInfo)
    {
        $original = sprintf(
            'https://farm%s.staticflickr.com/%s/%s_%s_o.jpg',
            (string)$photoInfo['farm'],
            (string)$photoInfo['server'],
            (string)$photoInfo['id'],
            (string)$photoInfo['secret']
        );

        $largeSquare = sprintf(
            'https://farm%s.staticflickr.com/%s/%s_%s_q.jpg',
            (string)$photoInfo['farm'],
            (string)$photoInfo['server'],
            (string)$photoInfo['id'],
            (string)$photoInfo['secret']
        );

        $thumbnail = sprintf(
            'https://farm%s.staticflickr.com/%s/%s_%s_t.jpg',
            (string)$photoInfo['farm'],
            (string)$photoInfo['server'],
            (string)$photoInfo['id'],
            (string)$photoInfo['secret']
        );


        $large1024 = sprintf(
            'https://farm%s.staticflickr.com/%s/%s_%s_b.jpg',
            (string)$photoInfo['farm'],
            (string)$photoInfo['server'],
            (string)$photoInfo['id'],
            (string)$photoInfo['secret']
        );

        return compact('original', 'largeSquare', 'thumbnail', 'large1024');
    }
}
