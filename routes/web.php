<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GalleryController@index');

# Disable registration
Auth::routes(['register' => false]); 

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('/', function () { return redirect()->route('gallery.index'); });
    Route::resource('gallery', 'Admin\GalleryCategoryController');
//    Route::apiResource('gallery', 'Admin\GalleryCategoryController');
});

//Route::get('/home', 'HomeController@index')->name('home');
